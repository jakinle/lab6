/*
Alex Kinley
jakinle
Lab 5
Nushrat Humaira
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card cardArray[52];

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   //Returns a 0-3 value for suit and 0-12 for the value and assigns each to the deck
   for(int i = 0; i < 52; i++){
      switch(i % 4){
         case 0:
            cardArray[i].suit = SPADES;
            break;
         case 1:
            cardArray[i].suit = HEARTS;
            break;
         case 2:
            cardArray[i].suit = DIAMONDS;
            break;
         case 3:
            cardArray[i].suit = CLUBS;
            break;
      }

     
      cardArray[i].value = (i % 13) + 2;
      }
   //Shuffles deck based off of myrandom function   
   random_shuffle(&cardArray[0], &cardArray[52], myrandom);
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
   Card cardHand[5] = {cardArray[0], cardArray[1], cardArray[2], cardArray[3], cardArray[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     //Sorts cards by suit type by order of enum type created above
   sort(&cardHand[0],&cardHand[5],suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
   
   //Prints out hand and calls functions to get name for face cards and unicode suit
   for(int i = 0; i < 5; i++)
   {
   cout << setw(10) << right << get_card_name(cardHand[i]) << "of " << get_suit_code(cardHand[i]) << endl;   
   }

   return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
return(lhs.suit < rhs.suit);
}

//Returns unicode suit character for cout
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//Gets name of value so face cards and ace print correctly
string get_card_name(Card& c) {
  // IMPLEMENT`:
   switch (c.value) {
      case 2:     return "2 ";
      case 3:     return "3 ";
      case 4:     return "4 ";
      case 5:     return "5 ";
      case 6:     return "6 ";
      case 7:     return "7 ";
      case 8:     return "8 ";
      case 9:     return "9 ";
      case 10:    return "10 ";
      case 11:    return "Jack ";
      case 12:    return "Queen ";
      case 13:    return "King ";
      case 14:    return "Ace ";
      default:    return"";
   }
}


